BASE.require([
  "jQuery"
], function(){
  BASE.namespace("app");

  app.Plugboard = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var wrangler = null;
    var $pluginsContainer = $(tags['plugins-container']);
    var pluginHeight = 150;
    var pluginWidth = 150;
    var plugins = [];
    var pluginsAdded = 0;

    var $pluginPath = $(tags['plugin-path']);
    var $addPlugin = $(tags['add-plugin']);

    var addPlugin = function(plugin){
      var $plugin = $(plugin);
      var controller = $plugin.controller();
      $plugin.css("position", "absolute");
      $plugin.css("left", pluginsAdded * pluginWidth);
      $plugin.css("top", "0");
      $plugin.css("height", pluginHeight);
      $plugin.css("width", pluginWidth);
      $plugin.addClass("plugboard-plugin");
      pluginsAdded++;
      wrangler.register(controller);
    };

    self.init = function(pluginWrangler){
      wrangler = pluginWrangler;
      $pluginsContainer.children().each(function(){
        var element = this;
        if ($(element).controller() !== null) {
          plugins.push(element);
        }
      });

      plugins.forEach(addPlugin);
    };

    $addPlugin.on("click", function(event){
      var path = $pluginPath.val();
      BASE.web.components.createComponentAsync(path).then(function(component){
        $pluginsContainer.append(component);
        addPlugin(component);
      });
    });

  };
});
