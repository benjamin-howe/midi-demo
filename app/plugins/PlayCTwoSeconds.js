BASE.require([
  "jQuery"
], function(){
  BASE.namespace("app.plugins");

  app.plugins.PlayCTwoSeconds = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var $play = $(tags['play']);

    var output = null;

    self.setOutput = function(value) {
      output = value;
    }

    $play.on("click", function(){
      output.send(0x90, 60, 127);
      setTimeout(function(){
        output.send(0x80, 60, 0);
      }, 2000);

    });


  };
});
