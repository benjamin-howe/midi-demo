BASE.require([
  "jQuery"
], function(){
  BASE.namespace("app.plugins");

  var keyboardNoteMap = {
    "R":58,
    "F":59,
    "G":60,
    "Y":61,
    "H":62,
    "U":63,
    "J":64
  };

  app.plugins.Keyboard = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var $toggle = $(tags["toggle"]);

    var output = null;

    var downListenerStates = {
      "on": function(event){
        var key = String.fromCharCode(event.keyCode);
        output.noteOn(1, keyboardNoteMap[key], 127)
      },
      "off": function(){}
    };
    var upListenerStates = {
      "on": function(event){
        var key = String.fromCharCode(event.keyCode);
        output.noteOff(1, keyboardNoteMap[key], 0);
      },
      "off": function(){}
    };
    var currentListenerState = "off";

    self.setOutput = function(value){
      output = value;
    };

    $toggle.on("click", function(){
      if ($toggle.hasClass("on")) {
        $toggle.removeClass("on");
        currentListenerState = "off";
      } else {
        $toggle.addClass("on");
        currentListenerState = "on";
      }
    });

    $(document.body).keydown(function(event){
      if (output) {
        downListenerStates[currentListenerState](event);
      }
    });

    $(document.body).keyup(function(event){
      if(output) {
        upListenerStates[currentListenerState](event);
      }
    });

  };
});
