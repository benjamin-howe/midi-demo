BASE.require([
  "jQuery",
  "BASE.util.Observer"
], function(){
  BASE.namespace("app.plugins");

  app.plugins.Transpose = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var $amount = $(tags['amount']);

    var input = null;
    var output = null;
    var inputObserver = new BASE.util.Observer;

    self.setInput = function(value){
      input = value;
      inputObserver.dispose();
      inputObserver = input.observe().filter(function(event){
        return event.type == "noteOn" || event.type == "noteOff";
      });
      inputObserver.onEach(function(event){
        var amount = parseInt($amount.val());
        if (event.type === "noteOn") {
          output.noteOn(event.channel, event.note + amount, event.velocity);
        } else if (event.type === "noteOff") {
          output.noteOff(event.channel, event.note + amount, 0);
        }
      });

    };

    self.setOutput = function(value){
      output = value;
    };
  };
});
