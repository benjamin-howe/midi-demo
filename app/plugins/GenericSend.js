BASE.require([
  'jQuery'
], function(){
  BASE.namespace("app.plugins");

  app.plugins.GenericSend = function(elem, tags, services) {
    var self = this;
    var $elem = $(elem);
    var $button = $(tags['button']);
    var $inputCommand = $(tags['input-command']);
    var $inputArg1 = $(tags['input-arg1']);
    var $inputArg2 = $(tags['input-arg2']);

    var output = null;

    self.setOutput = function(value) {
        output = value;
    }

    $button.on("click", function(event){
      if(output) {
        output.send(parseInt($inputCommand.val()),
          parseInt($inputArg1.val()),
          parseInt($inputArg2.val())
        );

        
      }
    });

  }
});
