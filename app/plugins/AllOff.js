BASE.require([
  "jQuery"
], function(){
  BASE.namespace("app.plugins");

  app.plugins.AllOff = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var $button = $(tags['button']);
    var output = null;

    self.setOutput = function(value){
      output = value;
    };

    $button.on("click", function(){
      if (output) {
        for (var i = 0; i < 12; i++) {
          var commandRoot = 0xB0;
          var command = commandRoot | i;
          var arg1 = 123;
          var arg2 = 0;
          output.send(command, arg1, arg2);
        }
      }
    });
  };
});
