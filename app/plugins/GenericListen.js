BASE.require([
  "jQuery",
  "BASE.util.Observer"
], function(){
  BASE.namespace("app.plugins");

  app.plugins.GenericListen = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var $command = $(tags['command']);
    var $arg1 = $(tags['arg1']);
    var $arg2 = $(tags['arg2']);
    var $display = $(tags['display']);
    var eventCount = 0;

    var input = null;
    var inputObserver = new BASE.util.Observer();

    var inputHandler = function(event){
      eventCount += 1;
      $display.text(eventCount);
    };

    self.setInput = function(value){
      input = value;
      inputObserver.dispose();
      inputObserver = input.observe().filter(function(event){
        var commandPass = true;
        var arg1Pass = true;
        var arg2Pass = true;
        if ($command.val() !== "*") {
            var commandFilter = parseInt($command.val());
            commandPass = event.command === commandFilter;
        }

        if ($arg1.val() !== "*") {
          var arg1Filter = parseInt($arg1.val());
          arg1Pass = event.arg1 === arg1Filter;
        }

        if ($arg2.val() !== "*") {
          var arg2Filter = parseInt($arg2.val());
          arg2pass = event.arg2 === arg2Filter;
        }

        return commandPass && arg1Pass && arg2Pass;

      }).onEach(inputHandler);
    };
  };
});
