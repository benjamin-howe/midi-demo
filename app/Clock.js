BASE.require([
  'jQuery',
  'BASE.util.Observable'
], function(){
  BASE.namespace("app");

  app.Clock = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var $display = $(tags['display']);
    var $tempo = $(tags['tempo']);
    var $reset = $(tags['reset']);
    var $playPause = $(tags['playpause']);
    var clockInterval = 0;

    BASE.util.Observable.apply(self);

    var timeIndex = 0;

    Object.defineProperty(self, "timeIndex", {
      set: function(value){},
      get: function(){
        return timeIndex;
      }
    });

    var tempo = 120;

    Object.defineProperty(self, "temp", {
      set: function(value){},
      get: function(){
        return tempo;
      }
    });

    var tick = function(){
      timeIndex++;
      self.notify({type:"tick", timeIndex:timeIndex});
      $display.text(timeIndex);
    };

    var getIntervalDelay = function(){
      var beatsPerMinute = tempo;
      var beatsPerSecond = tempo / 60;
      // 64th note / 4[beat = quarter note] = 16
      return (1000/16) / beatsPerSecond; // number of milliseconds to a 64th note
    };

    var startInterval = function(){
      if (typeof clockInterval === "number") {
        clearInterval(clockInterval);
      }
      clockInterval = setInterval(tick, getIntervalDelay());
    };

    var stopInterval = function(){
      clearInterval(clockInterval);
      clockInterval = null;
    };

    var playPauseHandlers = {
      stopped: function(){
        playPauseHandler = playPauseHandlers.running;
        $playPause.text("Pause");
        startInterval();
        self.notify({type:"runStateChange", newState:"running"});
      },
      running: function(){
        playPauseHandler = playPauseHandlers.stopped;
        $playPause.text("Run");
        stopInterval();
        self.notify({type:"runStateChange", newState:"stopped"})
      }
    };

    var playPauseHandler = playPauseHandlers.stopped;

    $playPause.on("click", function(event){
      playPauseHandler();
    });

    $reset.on("click", function(){
      if (playPauseHandler === playPauseHandlers.running) {
        playPauseHandler();
      }
      timeIndex = 0;
      $display.text("0");
      self.notify({type:"reset"});
    });

    $tempo.on("change", function(){
      tempo = parseInt($tempo.val(), 10);
      self.notify({type:"tempoChange", newTempo: tempo});
      if (playPauseHandler === playPauseHandlers.running) {
        startInterval();
      }
    });

  };
});
