BASE.require([
  "jQuery",
  "BASE.util.Observable",
  "BASE.util.Observer"
], function(){
  BASE.namespace("app.plugins");

  var emptyFn = function(){};

  var MIDIEventCollection = function(){
    var self = this;
    var eventCount = 0;
    BASE.util.Observable.apply(self);

    self.events = {};
    self.add = function(event){
      self.events[event.timeIndex] = self.events[event.timeIndex] || [];
      self.events[event.timeIndex].push(event);
      eventCount++;
      self.notify({type: "added", eventCount:eventCount, event:event});
    };

    self.reset = function(){
      self.events = {};
      eventCount = 0;
      self.notify({type: "reset"});
    };

    self.get = function(timeIndex){
      return self.events[timeIndex];
    };

    self.export = function(){
      return JSON.stringify(self.events);
    };

    Object.defineProperty(self, "eventCount", {
      set: emptyFn,
      get: function(){
        return eventCount;
      }
    });
  };


  app.plugins.Recorder = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var clock = null;
    var input = null;
    var output = null;
    var events = new MIDIEventCollection();

    var $armRecord = $(tags['arm-record']);
    var $armPlay = $(tags['arm-play']);
    var $reset = $(tags['reset']);
    var $channel = $(tags['channel']);
    var $disarm = $(tags['disarm']);
    var $eventCounter = $(tags['event-counter']);

    var clockStateObserver = new BASE.util.Observer();
    var clockPlaybackObserver = new BASE.util.Observer();
    var inputObserver = new BASE.util.Observer();

    var currentChannel = 1;

    events.observeType("added", function(event){
      $eventCounter.text(event.eventCount);
    });

    events.observeType("reset", function(event){
      $eventCounter.text("0");
    });

    var clockStateHandler = function(event){
      if (event.newState === "running") {
        inputObserver.start();
      } else if (event.newState === "stopped") {
        inputObserver.stop();
      }
    };

    var inputStates = {
      "disarmed":emptyFn,
      "record":function(event){
        event.timeIndex = clock.timeIndex;
        events.add(event);
      },
      "play":emptyFn
    };

    var playbackStates = {
      "disarmed": emptyFn,
      "record": emptyFn,
      "play": function(event){
        var eventArray = events.get(event.timeIndex);
        if (eventArray) {
          eventArray.forEach(function(event){
            if (event.type === "noteOn") {
              output.noteOn(currentChannel, event.note, event.velocity);
            } else if (event.type === "noteOff") {
              output.noteOff(currentChannel, event.note, event.velocity);
            }
          });
        }
      }
    };

    var currentState = "disarmed";

    self.setClock = function(value){
      clock = value;
      clockStateObserver.dispose();
      clockStateObserver = clock.observe();
      clockStateObserver.filter(function(event){
         return event.type === "runStateChange";
       }).onEach(clockStateHandler);

      clockPlaybackObserver.dispose();
      clockPlaybackObserver = clock.observe();
      clockPlaybackObserver.filter(function(event){
        return event.type === "tick";
      }).onEach(function(event){
        playbackStates[currentState](event);
      });
    };

    self.setInput = function(value) {
      input = value;
      inputObserver.dispose();
      inputObserver = input.observe();
      inputObserver.filter(function(event){
        return (event.type === "noteOn" || event.type === "noteOff") && event.channel === currentChannel;
      }).onEach(function(event){
        inputStates[currentState](event);
      });
      inputObserver.stop();
    }

    self.setOutput = function(value){
      output = value;
    };

    self.export = function(){
      return events.export();
    };

    $armRecord.on("click", function(){
      currentState = "record";
      $armRecord.addClass("armed");
      $armPlay.removeClass("armed");
      inputObserver.start();
    });

    $armPlay.on("click", function(){
      currentState = "play";
      $armPlay.addClass("armed");
      $armRecord.removeClass("armed");
      inputObserver.stop();
    });

    $disarm.on("click", function(){
      currentState = "disarmed";
      $armRecord.removeClass("armed");
      $armPlay.removeClass("armed");
      inputObserver.stop();
    });

    $reset.on("click", function(){
      currentState = "disarmed";
      events.reset();
    });

    $channel.on("change", function(){
      currentChannel = parseInt($channel.val(), 10);
    });

  };
});
