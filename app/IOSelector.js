BASE.require([
  'jQuery',
  'BASE.util.Observable',
  'BASE.collections.Hashmap'
], function(){
  BASE.namespace("app");

  app.IOSelector = function(elem, tags, services){
    var self = this;
    BASE.util.Observable.apply(self);
    var $inputs = $(tags['inputs']);
    var $outputs = $(tags['outputs']);
    var nullOption = '<option value="none">Select...</option>';

    var namesToInputs = new BASE.collections.Hashmap();
    var namesToOutputs = new BASE.collections.Hashmap();

    var setUpSelect = function($select, values){
      $('option', $select).remove();
      $select.append(nullOption);
      values.forEach(function(input){
        var $newOption = $('<option></option>');
        $newOption.attr("value", input.name);
        $newOption.text(input.name);
        $select.append($newOption);
      });
    };

    self.setInterfaces = function(interfaces) {
      namesToInputs = new BASE.collections.Hashmap();
      interfaces.inputs.forEach(function(i){
        namesToInputs.add(i.name, i);
      });
      setUpSelect($inputs, interfaces.inputs);
      namesToOutputs = new BASE.collections.Hashmap();
      interfaces.outputs.forEach(function(i){
        namesToOutputs.add(i.name, i);
      });
      setUpSelect($outputs, interfaces.outputs);
    };

    $inputs.on("change", function(){
      var newInput = $inputs.val() === "none" ? null : namesToInputs.get($inputs.val());
      self.notify({type:"inputChange", input:newInput});
    });

    $outputs.on("change", function(){
      var newOutput = $outputs.val() === "none" ? null : namesToOutputs.get($outputs.val());
      self.notify({type:"outputChange", output:newOutput});
    });

  };

});
