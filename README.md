# README #


### A Demo of the Chrome MIDI API ###
Runs only on Chrome.

### Set-up ###
The MIDI part of the demo itself requires no setup outside of hooking your MIDI equipment up to your computer like you would with any DAW, but the BASE Components application framework under the app loads local scripts dynamically, which Chrome doesn't allow for files served from the file:/// schema for security reasons. The repo comes with a simple node/express server to make it easy to play with.

* Clone the repo
* run `npm install` so you can use the local server
* run `node server.js`
* note the address at which the app is being served and point Chrome in that direction.