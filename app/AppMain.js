BASE.require([
  'jQuery',
  'midiDemo.MIDI'
], function(){
  BASE.namespace("app");

  app.AppMain = function(elem, tags, services) {
    var self = this;
    var $elem = $(elem);
    var plugboardController = $(tags['plugboard']).controller();
    var clock = $(tags['clock']).controller();
    var ioSelector = $(tags['io-selector']).controller();
    var defaultInput = null;
    var defaultOutput = null;
    var inputChangeObserver = null;
    var outputChangeObserver = null;

    var midi = new midiDemo.MIDI();

    var plugins = [];
    var wrangler = {
      register: function(plugin){
        // registered plugins are notified of default device changes
        if (plugins.indexOf(plugin) < 0) {
          plugins.push(plugin);
          if (clock && typeof plugin.setClock === "function") {
            plugin.setClock(clock);
          }
          if (defaultInput && typeof plugin.setInput === "function") {
            plugin.setInput(defaultInput);
          }
          if (defaultOutput && typeof plugin.setOutput === "function") {
            plugin.setOutput(defaultOutput);
          }
        }
      },
      unregister: function(plugin){
        if (plugins.indexOf(plugin) >= 0) {
          // plugins expected to dispose their observers before unregistering
          plugins = plugins.splice(plugins.indexOf(plugin), 1);
        }
      }
    };

    services.add("wrangler", wrangler);

    midi.initAsync().then(function(){
      ioSelector.setInterfaces(midi.listInterfaces());
      services.add("midi", midi);
      plugboardController.init(wrangler);
    });

    inputChangeObserver = ioSelector.observe().filter(function(event){
      return event.type === "inputChange";
    }).onEach(function(event){
      defaultInput = event.input;
      plugins.forEach(function(plugin){
        if (typeof plugin.setInput === "function") {
          plugin.setInput(event.input);
        }
      });
    });

    outputChangeObserver = ioSelector.observe().filter(function(event){
      return event.type === "outputChange";
    }).onEach(function(event){
      defaultOutput = event.output;
      plugins.forEach(function(plugin){
        if (typeof plugin.setOutput === "function") {
          plugin.setOutput(event.output);
        }
      });
    });

  };

});
