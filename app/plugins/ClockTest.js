BASE.require([
  "jQuery",
  "BASE.util.Observer"
], function(){
  BASE.namespace("app.plugins");

  app.plugins.ClockTest = function(elem, tags, services){
    var self = this;
    var $elem = $(elem);
    var $display = $(tags['clock']);
    var clock = null;
    var clockObserver = new BASE.util.Observer();


    self.setClock = function(value){
      clock = value;
      clockObserver.dispose();
      clockObserver = clock.observe();

      clockObserver.filter(function(e){return e.type === "tick"}).onEach(function(e){
        $display.text(e.timeIndex);
      });

      clockObserver.filter(function(e){
        return e.type === "reset";
      }).onEach(function(e){
        $display.text("");
      });
    };

  };
});
