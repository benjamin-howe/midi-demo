BASE.require([
  "BASE.async.Future",
  "BASE.util.Observable"
], function(){
  // thanks to http://www.keithmcmillen.com/blog/making-music-in-the-browser-web-midi-api/

  // message overview: https://www.midi.org/specifications/item/table-1-summary-of-midi-message

  BASE.namespace("midiDemo");

  midiDemo.noteMap = (function(){
    var notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];
    var map = {};
    var octave = -2;
    var note = 0;
    var noteNumber = 0;
    while (octave <= 8) {
      while (note <= 11) {
        map[notes[note] + octave] = noteNumber;
        noteNumber += 1;
        note += 1;
      }
      note = 0;
      octave += 1;
    }
    return map;
  })();

  midiDemo.MIDIInput = function(nativeInput){
    var self = this;
    BASE.util.Observable.apply(self);
    self.nativeInput = nativeInput;
    self.name = nativeInput.name;
    self.state = nativeInput.state;
    var typeMap = {
      0x90: "noteOn",
      0x80: "noteOff"
    };

    nativeInput.onmidimessage = function(message){
      var data = message.data;
      var type = data[0] & 0xf0; // get the command from the command byte
      var channel = (data[0] & 0x0f) + 1; // get the channel from the command byte, 1-index it
      var niceType = typeMap[type];
      var note = data[1];
      var velocity = data[2];
      if (niceType === "noteOn" && velocity === 0) {
          niceType = "noteOff";
          type = 0x80;
      }
      self.notify({type:niceType, note:note, velocity:velocity, channel:channel, command:data[0], arg1:data[1], arg2:data[2]});
    };

  };

  midiDemo.MIDIOutput = function(nativeOutput) {
    var self = this;
    self.nativeOutput = nativeOutput;
    self.name = nativeOutput.name;
    self.state = nativeOutput.state;

    self.send = function(command, arg1, arg2){
      if (typeof arg2 === undefined) {
        self.nativeOutput.send([command, arg1]);
      } else {
        self.nativeOutput.send([command, arg1, arg2]);
      }
    };

    // NoteOn = 1001cccc, 0nnnnnnn, 0vvvvvvv
    // Channel is 1-indexed
    self.noteOn = function(channel, note, velocity) {
      velocity = velocity || 100;
      channel = channel - 1;
      var command = 0x90 | channel;
      self.send(command, note, velocity);
    }

    self.noteOff = function(channel, note, velocity) {
      velocity = velocity || 0;
      channel = channel - 1;
      var command = 0x80 | channel;
      self.send(command, note, velocity);
    }

  }

  midiDemo.MIDI = function(){
    var self = this;
    var Future = BASE.async.Future;

    if (!navigator.requestMIDIAccess) {
      throw new Error("MIDI is not available in this browser");
    }
    self.midiAccess = null;

    self.initAsync = function(){
      return new Future(function(setValue, setError){
        navigator.requestMIDIAccess().then(function(midi){
          // success
          self.midiAccess = midi;
          setValue();
        }, function(error){
          // failure
          setError("Couldn't get MIDI access:" + error);
        });
      });
    };

    self.listInterfaces = function(){
      if (self.midiAccess === null) {
        throw new Error("Run initAsync() first.");
      }
      var ifaces = {inputs:[], outputs:[]};
      var inputs = self.midiAccess.inputs.values();
      for (var input = inputs.next(); !input.done; input = inputs.next()) {
        ifaces.inputs.push(new midiDemo.MIDIInput(input.value));
      }
      var outputs = self.midiAccess.outputs.values();
      for (var output = outputs.next(); !output.done; output = outputs.next()) {
        ifaces.outputs.push(new midiDemo.MIDIOutput(output.value));
      }
      return ifaces;
    };

  };

});
